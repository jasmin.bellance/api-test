FROM ruby:2.3-jessie
RUN apt-get update -y
RUN apt-get install nodejs -y
RUN mkdir -p /var/www/app/api
RUN gem install rails
COPY ./api /var/www/app/api
WORKDIR /var/www/app/api
# Follow line is to comment when first no rails source existing
RUN bundle install
RUN apt-get install telnet -y