README Jayk'One
-

Project installation
--
Create the project
``docker-compose up --build -d``

Get the app container id
``docker ps``

Load the dependencies and start the server
``docker exec -ti [container_id] bundle install && /bin/rails s &``

