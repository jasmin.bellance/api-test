Rails.application.routes.draw do
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pages#index'
  get   'welcome' => 'pages#welcome', :as => 'welcome'
  get   'error' => 'pages#error', :as => 'error'
  get   'signin' => 'users#signin', :as => 'signin'
  post  'auth' => 'users#auth', :as => 'auth'
  get   'login' => 'users#login', :as => 'login'
  get   'lost' => 'users#lost', :as => 'lost'
  post   'resetpwd' => 'users#resetpwd', :as => 'resetpwd'
  get   'confirm_signin/:nickname/:token' => 'users#confirm_signin', :as => 'confirm_signin'
  get   'about' => 'pages#about', :as => 'about'
  get   'robots.txt' => 'pages#robots', :as => 'robots'
end
