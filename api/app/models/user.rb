class MailValidator < ActiveModel::Validator
  def validate(record)
     if (User::MAIL_VALIDATION_RULE =~ record.email).nil?
      record.errors[:base] << "This email is not valid"
    end
  end
end


class User < ApplicationRecord
  has_secure_password
  before_create :generate_token
  MAIL_VALIDATION_RULE = /(.+)[@](.+)[.](.+){1,8}/

  validates :email, presence: true, uniqueness: true
  validates :nickname, uniqueness: true, case_sensitive: false
  validates :password_digest, presence: true
  validates_with MailValidator

  def generate_token
    self.token = SecureRandom.uuid
    # Save new tocken only for registered user => do not save user for new user, the save is done once, controller 
    self.save if self.persisted?
  end

  def activate
  	self.active = true
  end
end
