class UserMailer < ApplicationMailer
	default from: 'notifications@app.fr'
 	DELIVERY_OPTIONS = { 
		#user_name: params[:company].smtp_user,
	    #password: params[:company].smtp_password,
	    address: '10.0.2.15'
	}
	def welcome_email
		@user = params[:user]
		@url  = 'http://127.0.0.1:3000/confirm_signin/' + @user.nickname + "/" + @user.token
		mail(to: @user.email, subject: 'Welcome to Jayk\'One', delivery_method_options: DELIVERY_OPTIONS)
	end
end
