class ApplicationMailer < ActionMailer::Base
  default from: 'contact@app.fr'
  layout 'mailer'
end
