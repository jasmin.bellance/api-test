class AddMailAddressTokenActiveLastVisitAtSuscribedAtToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :mail_address, :string, uniq: true, null: false
  	add_column :users, :token, :string, uniq: true, null: false
  	add_column :users, :active, :boolean, uniq: true,  null: false, default: false
  	add_column :users, :last_visit_at, :timestamp, uniq: true
  	add_column :users, :subscribed_at, :timestamp, uniq: true
  end
end
