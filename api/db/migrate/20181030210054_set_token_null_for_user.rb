class SetTokenNullForUser < ActiveRecord::Migration[5.2]
  def change
  	change_column :users, :token, :string, null: true
  end
end
