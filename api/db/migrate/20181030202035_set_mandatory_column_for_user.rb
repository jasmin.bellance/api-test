class SetMandatoryColumnForUser < ActiveRecord::Migration[5.2]
  def change
  	change_column :users, :email, :string, null: false
  	change_column :users, :nickname, :string, uniq: true
  	remove_column :users, :mail_address
  end
end
